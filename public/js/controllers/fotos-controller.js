// USANDO A CONVENÇÃO DO ANGULAR CAMEL CASE NOS PARÂMETROS.
angular.module('alurapic').controller('FotosController',function($scope){

    // SCOPE SERVE PARA QUE O CONTROLLER DISPONIBILIZE DADOS PARA A VIEW.
    $scope.foto = {
        titulo : " Interior Mercedes Benz AMG ",
        url: 'https://i.pinimg.com/originals/a8/bc/1d/a8bc1d01a7209a21578ffb4e70ff4bf9.jpg'
    };



});